# WidgetList #

WidgetList is an automated tool to implement interactive UI elements called widgets within Jupyter notebooks.

WidgetList is essentially a widget that manages other widgets.